package geometry;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class TriangleTest {
    final double tolerance = 0.0000001;

    @Test
    public void testGetHeightBase(){
        Triangle triangle = new Triangle(3, 5);
        assertEquals(3, triangle.getBase(), tolerance);
        assertEquals(5, triangle.getHeight(), tolerance);
    }

    @Test
    public void testGetArea(){
        Triangle triangle = new Triangle(4, 6);
        assertEquals(12, triangle.getArea(), tolerance);
    }

}
