package geometry;

import javax.naming.directory.InvalidAttributesException;

public class Circle {
    private double radius;
    public Circle (double radius) throws InvalidAttributesException {
        if(radius>0) {
            this.radius=radius;
        }
        else {
            throw new InvalidAttributesException("Radius is not negative");
        }
    }
    public double getRadius() {
        return this.radius;
    }
    public double getCircumference() {
        return 2*radius*Math.PI;
    }
    public double getArea(){
        return Math.pow(radius,2)*Math.PI;
    }
    public String toString() {
        return "This is a circle with "+radius+" as radius";
	}
}
