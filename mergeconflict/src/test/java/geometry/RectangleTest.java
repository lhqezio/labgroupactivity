package geometry;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
public class RectangleTest {
    double margError = 0.000000000001;
    @Test
    public void constorTest(){
        Rectangle r = new Rectangle(2, 3);
        assertEquals(2, r.getLength(),margError);
        assertEquals(3, r.getWidth(),margError);
    }
    @Test
    public void areaTest(){
        Rectangle r = new Rectangle(2, 3);
        assertEquals(6, r.getArea(),margError);
    }
    @Test
    public void toStringTest(){
        Rectangle r = new Rectangle(2, 3);
        assertEquals("width is: 3.0length is: 2.0", r.toString());
    }
    
}
